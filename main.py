import os
import sqlite3
import sys

from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QHeaderView, QMainWindow, QMessageBox
import SqlViewerUi


class Model(QtCore.QAbstractTableModel):
    def __init__(self, table, headers):
        super().__init__()
        self.table = table
        self.headers = headers

    def rowCount(self, parent):
        return len(self.table)

    def columnCount(self, parent):
        return len(self.table[0]) if self.table else 0

    def data(self, index, role):
        if role == QtCore.Qt.DisplayRole:
            return self.table[index.row()][index.column()]

    def setData(self, index, value, role):
        if role == QtCore.Qt.EditRole:
            self.table[index.row()][index.column()] = value
        return True

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                headers_mapping = dict(enumerate(self.headers))
                return headers_mapping.get(section, section)
            elif orientation == QtCore.Qt.Vertical:
                return section


class Viewer(QMainWindow, SqlViewerUi.Ui_MainWindow):
    def __init__(self, default_connection: str, qs_limit: int):
        super().__init__()
        self.setupUi(self)
        self.db_name = default_connection
        self.qs_limit = qs_limit
        self.conClearButton.clicked.connect(self.clear_connect_input)
        self.queryInpClearButton.clicked.connect(self.clear_query_input)
        self.queryInpSendButton.clicked.connect(self.reconnect_db)
        self.queryInpSendButton.clicked.connect(self.execute_sql)

        self.connection_input.setPlainText(default_connection)
        self.conn = self.init_db(default_connection)

    def init_db(self, db_name):
        return sqlite3.connect(db_name)

    def reconnect_db(self):
        db_name = self.connection_input.toPlainText()
        if self.db_name != db_name:
            self.db_name = db_name
            if self.conn:
                self.conn.close()
            if os.path.isfile(db_name) or db_name == ':memory:':
                self.conn = self.init_db(db_name)
            else:
                self.show_info_msg('Wrong database connection input', 'Error')
                self.conn = None

    def clear_connect_input(self):
        self.connection_input.clear()

    def clear_query_input(self):
        self.query_input.clear()

    def execute_sql(self):
        sql_query = self.query_input.toPlainText()
        if not sql_query:
            self.show_info_msg('Input is empty!', 'Info')
            return
        headers, qs = [], []
        if self.conn:
            sql_error_msg, cur = None, None
            try:
                cur = self.conn.execute(sql_query)
                if cur.description:
                    headers = [x[0] for x in cur.description]
                for row in cur:
                    if len(qs) < self.qs_limit:
                        qs.append(row)
                    else:
                        sql_error_msg = 'Query is too large and cannot be processed completely'
                        break
            except sqlite3.DatabaseError as e:
                sql_error_msg = str(e)
            self.process_msgs(len(qs), cur, sql_error_msg)
        self.display_result(qs, headers)

    def process_msgs(self, qs_len, cur, error_msg):
        msg, title = None, None
        if error_msg:
            msg = error_msg
            title = 'Error'
        else:
            title = "Info"
            if cur.description and qs_len == 0:
                msg = 'Result is empty!'
            elif not cur.description:
                msg = '%d row(s) were processed' % cur.rowcount

        if msg and title:
            self.show_info_msg(msg, title)

    def display_result(self, qs, headers):
        self.model = Model(qs, headers)
        self.ResultView.setModel(self.model)
        self.ResultView.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

    def show_info_msg(self, msg, title):
        mes = QMessageBox()
        mes.setText(msg)
        mes.setWindowTitle(title)
        mes.exec()

    def closeEvent(self, event):
        self.conn.close()
        app.quit()


if __name__ == '__main__':
    DEFAULT_CONNECTION = ':memory:'
    QS_LIMIT = 10000
    app = QApplication(sys.argv)
    window = Viewer(DEFAULT_CONNECTION, QS_LIMIT)
    window.show()
    app.exec_()
