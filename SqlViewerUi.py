# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SqlViewer.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class ModifiedQTableView(QtWidgets.QTableView):

    def resizeEvent(self, event):
        super().resizeEvent(event)
        header = self.horizontalHeader()
        for column in range(header.count()):
            header.setSectionResizeMode(column, QtWidgets.QHeaderView.ResizeToContents)
            width = header.sectionSize(column)
            header.setSectionResizeMode(column, QtWidgets.QHeaderView.Interactive)
            header.resizeSection(column, width)


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(593, 532)
        MainWindow.setMinimumSize(QtCore.QSize(600, 600))
        MainWindow.setMaximumSize(QtCore.QSize(600, 600))
        MainWindow.setWindowTitle("SQL Viewer")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.connection_input = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.connection_input.setGeometry(QtCore.QRect(20, 30, 271, 41))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(18)
        self.connection_input.setFont(font)
        self.connection_input.setPlainText("")
        self.connection_input.setObjectName("connection_input")
        self.connection_label = QtWidgets.QLabel(self.centralwidget)
        self.connection_label.setGeometry(QtCore.QRect(20, 10, 111, 16))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        font.setBold(False)
        font.setWeight(50)
        self.connection_label.setFont(font)
        self.connection_label.setObjectName("connection_label")
        self.query_input = QtWidgets.QPlainTextEdit(self.centralwidget)
        self.query_input.setGeometry(QtCore.QRect(20, 130, 471, 81))
        self.query_input.setObjectName("query_input")
        self.queryInpSendButton = QtWidgets.QPushButton(self.centralwidget)
        self.queryInpSendButton.setGeometry(QtCore.QRect(510, 180, 51, 31))
        self.queryInpSendButton.setObjectName("queryInpSendButton")
        self.query_input_label = QtWidgets.QLabel(self.centralwidget)
        self.query_input_label.setGeometry(QtCore.QRect(20, 100, 111, 21))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        self.query_input_label.setFont(font)
        self.query_input_label.setObjectName("query_input_label")
        self.ResultView = ModifiedQTableView(self.centralwidget)
        self.ResultView.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.ResultView.setGeometry(QtCore.QRect(20, 270, 551, 241))
        self.ResultView.setMinimumSize(QtCore.QSize(551, 241))
        self.ResultView.setMaximumSize(QtCore.QSize(551, 241))
        self.ResultView.setToolTipDuration(600)
        self.ResultView.setObjectName("ResultView")
        self.conClearButton = QtWidgets.QPushButton(self.centralwidget)
        self.conClearButton.setGeometry(QtCore.QRect(310, 40, 61, 21))
        self.conClearButton.setObjectName("conClearButton")
        self.queryInpClearButton = QtWidgets.QPushButton(self.centralwidget)
        self.queryInpClearButton.setGeometry(QtCore.QRect(510, 130, 51, 31))
        self.queryInpClearButton.setObjectName("queryInpClearButton")
        self.result_label = QtWidgets.QLabel(self.centralwidget)
        self.result_label.setGeometry(QtCore.QRect(20, 240, 61, 21))
        font = QtGui.QFont()
        font.setFamily("Times New Roman")
        font.setPointSize(16)
        self.result_label.setFont(font)
        self.result_label.setObjectName("result_label")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.connection_label.setText(_translate("MainWindow", "Connection:"))
        self.queryInpSendButton.setText(_translate("MainWindow", "Send"))
        self.query_input_label.setText(_translate("MainWindow", "Query input:"))
        self.conClearButton.setText(_translate("MainWindow", "Clear"))
        self.queryInpClearButton.setText(_translate("MainWindow", "Clear"))
        self.result_label.setText(_translate("MainWindow", "Result:"))
